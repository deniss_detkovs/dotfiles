curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
sudo add-apt-repository ppa:neovim-ppa/unstable -y
sudo apt-get update
sudo apt-get install -y zsh git nodejs build-essential neovim python-dev python-pip python3-dev python3-pip
sudo pip3 install neovim

sudo sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone git@bitbucket.org:deniss_detkovs/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
git checkout work-virtual
git submodule update --init
cd ~/